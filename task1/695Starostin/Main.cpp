﻿#define GLM_ENABLE_EXPERIMENTAL
#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <glm/vec3.hpp>
#include <Perlin.h> //
#include <iostream>

class Point
{
public:
	glm::vec3 coord;
	glm::vec3 normal;
	Point(float X, float Y, float Z, float NX, float NY, float NZ)
		:coord(X, Y, Z), normal(NX, NY, NZ) {};
	Point()
		:Point(0, 0, 0, 0, 0, 0) {};
	Point(float* mem)
		:Point(mem[0], mem[1], mem[2], mem[3], mem[4], mem[5]) {};
	void write(float* mem) const
	{
		mem[0] = coord.x;
		mem[1] = coord.y;
		mem[2] = coord.z;
		mem[3] = normal.x;
		mem[4] = normal.y;
		mem[5] = normal.z;
	}
};

struct Landscape
{
public:
	float* pointData = 0; //Each point has 6 values: coords and normal vector
    unsigned int* indices = 0;
	unsigned int width;
	unsigned int height;
    float coordScaling = 0.1;
    unsigned int NVertices()
    {
        return width * height;
    }
    unsigned int NPolygons()
    {
        return 2 * (width - 1) * (height - 1);
    }
    float pointX(int j)
    {
        return  coordScaling * (-0.5 * height + j);
    }
    float pointY(int i)
    {
        return coordScaling * (-0.5 * width + i);
    }
	Perlin perlinGen;
	Landscape(unsigned int Width, unsigned int Height) 
		:perlinGen(), width(Width), height(Height)
	{
		pointData = new float[6 * NVertices()];
        indices = new unsigned int[3 * NPolygons()];
	};
	unsigned long long ind(long long int i, int j, unsigned int h) const
	{
		return i * h + j;
	}
	Point pointAt(int i, int j)
	{
		if (i < 0 || i >= width || j < 0 || j >= height)
		{
			return Point(pointY(i), pointX(j), 0, 0, 0, 0);
		}
		return Point(pointData + 6 * ind(i, j, height));
	}
	void writePointAt(const Point& p, int i, int j)
	{
		p.write(pointData + 6 * ind(i, j, height));
	}
	void computeNormalAt(int i, int j)
	{
		Point p1 = pointAt(i, j), p2 = pointAt(i + 1, j), p3 = pointAt(i, j + 1);
		p1.normal = glm::normalize(glm::cross(p2.coord - p1.coord, p3.coord - p1.coord));
		writePointAt(p1, i, j);
	}
	void generateCoords()
	{
		for (int i = 0; i < width; ++i)
		{
			for (int j = 0; j < height; ++j)
			{
                float pointy = pointY(i), pointx = pointX(j);
				writePointAt(Point(pointx, pointy, (1 + perlinGen.noise(pointx, pointy, 0)) / 2, 0, 0, 0), i, j);
			}
		}
		for (int i = 0; i < width; ++i)
		{
			for (int j = 0; j < height; ++j)
			{
				computeNormalAt(i, j);
                Point p = pointAt(i, j);
			}
		}
	}
    void generateIndices()
    {
        for (int i = 0; i < width - 1; ++i)
        {
            for (int j = 0; j < height - 1; ++j)
            {
                unsigned long long index = 6 * ind(i, j, height - 1);

                indices[index] = ind(i, j, height);
                indices[index+1] = ind(i, j+1, height);
                indices[index+2] = ind(i+1, j, height);
                indices[index+3] = ind(i, j+1, height);
                indices[index+4] = ind(i+1, j, height);
                indices[index+5] = ind(i+1, j+1, height);
            }
        }
    }

    ~Landscape()
    {
        delete pointData;
        delete indices;
    }

};

class LandscapeApplication : public Application {
public:
    //Идентификатор VertexArrayObject, который хранит настройки полигональной модели
    GLuint _vao;

    //Идентификатор шейдерной программы
    GLuint _program;

    ShaderProgramPtr _shader;

    unsigned int _indexCount;
    void makeScene() override
    {
        Application::makeScene();
        Landscape landscape(2500, 1075);
        landscape.generateCoords();
        landscape.generateIndices();
        _indexCount = landscape.NPolygons() * 3;
        
        //Создаем буфер VertexBufferObject для хранения координат на видеокарте
        GLuint vbo;
        glGenBuffers(1, &vbo);

        //Делаем этот буфер текущим
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        //Копируем содержимое массива в буфер на видеокарте
        glBufferData(GL_ARRAY_BUFFER, 6 * landscape.NVertices() * sizeof(float), landscape.pointData, GL_STATIC_DRAW);

        //Создаем ещё один буфер VertexBufferObject для хранения индексов
        unsigned int ibo;
        glGenBuffers(1, &ibo);

        //Делаем этот буфер текущим
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

        //Копируем содержимое массива индексов в буфер на видеокарте
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indexCount * sizeof(unsigned int), landscape.indices, GL_STATIC_DRAW);

        //Создаем объект VertexArrayObject для хранения настроек полигональной модели
        glGenVertexArrays(1, &_vao);

        //Делаем этот объект текущим
        glBindVertexArray(_vao);

        //Делаем буфер с координатами текущим
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        //Включаем 0й вершинный атрибут - координаты
        glEnableVertexAttribArray(0);

        //Включаем 1й вершинный атрибут - нормали
        glEnableVertexAttribArray(1);

        //Устанавливаем настройки:
        //0й атрибут,
        //3 компоненты типа GL_FLOAT,
        //не нужно нормализовать,
        //28 - расстояние в байтах между 2мя соседними значениями,
        //0 - сдвиг от начала
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), reinterpret_cast<void*>(0));

        //Устанавливаем настройки:
        //1й атрибут,
        //3 компоненты типа GL_FLOAT,
        //не нужно нормализовать,
        //28 - расстояние в байтах между 2мя соседними значениями,
        //12 - сдвиг в байтах от начала массива
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), reinterpret_cast<void*>(3 * sizeof(float)));

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBindVertexArray(0);

        //=========================================================

        _shader = std::make_shared<ShaderProgram>("695StarostinData1/shader.vert", "695StarostinData1/shader.frag");
    }

    void draw() override
    {
        Application::draw();

        //Получаем размеры экрана (окна)
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер		
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Подключаем VertexArrayObject с настойками полигональной модели
        glBindVertexArray(_vao);

        //Рисуем полигональную модель (состоит из треугольников, сдвиг 0, количество вершин 3)
        glDrawElements(GL_TRIANGLES, _indexCount, GL_UNSIGNED_INT, 0); //Рисуем с помощью индексов
    }
	
	LandscapeApplication()
		:Application()
	{}	
};

int main() {
	LandscapeApplication app;
	app.start();
	return 0;
}
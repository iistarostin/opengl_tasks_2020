#version 330

uniform sampler2D snow;
uniform sampler2D dirt;
uniform sampler2D grass;
uniform sampler2D rock;
uniform sampler2D base;

in vec3 normalCamSpace;
in vec4 posCamSpace;
in vec4 lightDirCamSpace;
in vec2 texCoord;

out vec4 fragColor;

const vec3 La = vec3(1.0, 1.0, 1.0) * 0.1;
const vec3 Ld = vec3(1.0, 1.0, 1.0) * 0.5;
const vec3 Ls = vec3(1.0, 1.0, 1.0) * 0.1;

const vec3 Ks = vec3(1.0, 1.0, 1.0);
const float shininess = 128.0;


void main()
{
    vec4 coeff = normalize(texture(base, texCoord / 10.0));
    vec3 diffuseColor = texture(snow, texCoord).rgb * coeff[0] +
                        texture(dirt, texCoord).rgb * coeff[1] +
                        texture(grass, texCoord).rgb * coeff[2] +
                        texture(rock, texCoord).rgb * coeff[3];

    vec3 normal = normalize(normalCamSpace);
	vec3 viewDirection = normalize(posCamSpace.xyz);

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0);

	vec3 color = diffuseColor * (La + Ld * NdotL);
	
	if (NdotL > 0.0)
	{
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);

		float blinnTerm = max(dot(normal, halfVector), 0.0);
		blinnTerm = pow(blinnTerm, shininess * length(diffuseColor)); //shininess changes based on texture color brightness
		color += Ls * Ks * blinnTerm;
	}
	
	fragColor = vec4(color, 1.0);
}

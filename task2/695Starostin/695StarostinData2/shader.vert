#version 330

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalToCameraMatrix;
uniform vec4 lightDir;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec3 normalCamSpace;
out vec4 posCamSpace;
out vec2 texCoord;
out vec4 lightDirCamSpace;

void main()
{
	texCoord = vec2(vertexPosition.x, vertexPosition.y);
	
	lightDirCamSpace = viewMatrix * normalize(lightDir);
	posCamSpace = viewMatrix * vec4(vertexPosition, 1.0);
	normalCamSpace = normalize(normalToCameraMatrix * vertexNormal);

	gl_Position = projectionMatrix * viewMatrix * vec4(vertexPosition, 1.0);
}